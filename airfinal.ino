#include <lmic.h>
#include <hal/hal.h>
#include "DHT.h"

#include <SPI.h>
#include "hackAir.h"
#include <math.h>

#define loraSerial Serial1
#define debugSerial Serial
#define DHTPIN 3
#define DHTTYPE DHT22

// Replace REPLACE_ME with TTN_FP_EU868 or TTN_FP_US915
#define freqPlan TTN_FP_IN865_867

byte payload[16];

float WarningValue=19.5;
//The minimum sate concentration of O2 in air

DHT dht(DHTPIN, DHTTYPE);
hackAIR sensor(SENSOR_PMS5003);

static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

static const u4_t DEVADDR = 0x00000000 ; 

void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }



struct hackAirData pollution;
struct data {
  short int temp;
  short int hum;
  short int pm25;
  short int pm10;
  float o2Concentration;
}mydata;

static osjob_t sendjob;

const unsigned TX_INTERVAL = 60;

const lmic_pinmap lmic_pins = {
    .nss = 8,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 4,
    .dio = {7,2,LMIC_UNUSED_PIN},
};

void onEvent (ev_t ev) {
 // Serial.print(os_getTime());
  //Serial.print(": ");
  switch (ev) {
  
    case EV_TXCOMPLETE:
      //Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if (LMIC.txrxFlags & TXRX_ACK)
        //Serial.println(F("Received ack"));
      if (LMIC.dataLen) {
        //Serial.println(F("Received "));
       // Serial.println(LMIC.dataLen);
        //Serial.println(F(" bytes of payload"));
      }
      // Schedule next transmission
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
      break;   
    default:
    //  Serial.println(F("Unknown event"));
      break;
  }
}

void read_sensors(){
  
      float sensorValue;
      float sensorVoltage;
      uint16_t Value_O2;
      sensorValue=analogRead(A0);
      sensorVoltage=(sensorValue/1024)*3.3;
      sensorVoltage=sensorVoltage/201*10000;
      Value_O2=(sensorVoltage/7.43)*100;
      mydata.o2Concentration= Value_O2;
      // mydata.h= lowByte(Value_O2);

      Serial.print("Concentration of O2 is ");
      Serial.print(sensorVoltage/7.43);
      Serial.println("%");


      // if(Value_O2<=WarningValue){
      //   digitalWrite(13,HIGH);
      //   }else
      //   digitalWrite(13,LOW);


      delay(300);

      mydata.temp = dht.readTemperature();
      mydata.hum = dht.readHumidity();

      Serial.println(dht.readTemperature());
      Serial.println(dht.readHumidity());
      
      delay(100);

      sensor.readData(pollution);
      if (pollution.error != 0) {
        //Serial.println("Error!");
      } else {
        
        Serial.print(pollution.pm25);
        Serial.print(",");
        Serial.println(pollution.pm10);
      }
      // float c=pollution.pm25;
      // float d=pollution.pm10;

      mydata.pm25 = pollution.pm25;
      mydata.pm10 = pollution.pm10; 
}
void do_send(osjob_t* j) {

      //Serial.println("hello");
      // Check if there is not a current TX/RX job running
      if (LMIC.opmode & OP_TXRXPEND) {
      //  Serial.println(F("OP_TXRXPEND, not sending"));
      } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
      //  Serial.println(F("Packet queued"));
      }
    }


void setup() {
  pinMode(12,OUTPUT);
  digitalWrite(12,LOW);
  sensor.begin();
  Serial.begin(9600);

  dht.begin();

//  Serial.println(F("Starting"));
//  while (!Serial && millis() < 1000000);
  #ifdef VCC_ENABLE
   For Pinoccio Scout boards
   
  pinMode(VCC_ENABLE, OUTPUT);
  digitalWrite(VCC_ENABLE, HIGH);
  delay(100);
#endif

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
#ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
#else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
#endif

#if defined(CFG_eu868)
 
  LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
 
#elif defined(CFG_us915)
 
  LMIC_selectSubBand(1);
#endif

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;

  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7, 14);

  // Start job
  do_send(&sendjob);
}

void loop() {
  os_runloop_once();
 

}
