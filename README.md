# LoRaWAN Based Air Quality Monitor

Urban environments with a high degree of industrialization are infested with hazardous chemicals and airborne pollutants. These pollutants can have devastating effects on human health, causing both acute and chronic diseases such as respiratory infections, lung cancer, and heart disease. Air pollution monitoring is vital not only to citizens, warning them on the health risks of air pollutants, but also to policy-makers, assisting them on drafting regulations and laws that aim at minimizing those health risks. Currently, air pollution monitoring predominantly relies on expensive high-end static sensor stations. These stations produce only aggregated information about air pollutants, and are unable to capture variations in individual's air pollution exposure.
In this project, we will make a Air Quality Monitor Station that measures atmospheric temperature, humidity, air quality parameters like PM 1.0,PM 2.5, PM 10 and CO2 using the controller board [ULP Lora 2.1 board](https://gitlab.com/icfoss/OpenIoT/ulplora). The interfacing sensors are used to collect the various data
mentioned above and the board's inbuilt rf module used to send the data to the nearest gateway and this data
is pushed into the influx Db, which saves the data for data acquisition! The station is working by using a 5 V adaptor.As we work through the project we will connect up the various sensors. In this process, we create a software sketch that will run the Air Quality Monitor Station.

## Getting Started


- Make sure that you have a ULP LoRa Board.

- Install project library from [here](https://gitlab.com/arunchandra2500/ulplora_air_quality_monitor/-/tree/master/libraries) . Copy the library to ~/Arduino/libraries/

- Select board : Arduino Pro Mini 3.3v 8Mhz

## Prerequisites

Arduino IDE - 1.8.9 [Tested]

DHT22 Temperature Humidity Sensor : https://www.amazon.in/Generic-Digital-Temperature-Humidity-Sensor/dp/B00O8RIYYU

MH-Z19 Infrared CO2 Sensor:  https://robu.in/product/mh-z19-infrared-co2-sensor-module-co2-monitor/

PMS5003 Air Quality Sensor: https://www.amazon.com/gp/product/B07S5YX84W/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07S5YX84W&linkCode=as2&tag=shophow2elect-20&linkId=b0c3eeaaf111070f0402db682bc3dcfa


## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/arunchandra2500/ulplora_air_quality_monitor/-/blob/master/LICENSE.md) file for details

## Acknowledgments


- LMIC by [matthijskooijman](https://github.com/matthijskooijman/arduino-lmic)
